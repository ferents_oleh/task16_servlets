package com.epam.model.impl;

import com.epam.model.Pizza;

public class VeganPizza implements Pizza {
    private final String name = "Vegan pizza";

    private final double price = 225;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
