package com.epam.model.impl;

import com.epam.model.Pizza;

public class CheesePizza implements Pizza {
    private final String name = "Cheese pizza";

    private final double price = 230;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public double getPrice() {
        return price;
    }
}
