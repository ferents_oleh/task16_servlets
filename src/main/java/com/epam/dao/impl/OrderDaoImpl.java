package com.epam.dao.impl;

import com.epam.dao.OrderDao;
import com.epam.model.Customer;
import com.epam.model.Order;
import com.epam.model.impl.CarbonaraPizza;
import com.epam.model.impl.CheesePizza;

import java.util.SortedMap;
import java.util.TreeMap;

public class OrderDaoImpl implements OrderDao {
    private SortedMap<Long, Order> orders;

    {
        orders = new TreeMap<>() {
            {
                Order order = new Order(new Customer("Howard", "Burns", "(730) 541-9050"),
                        new CheesePizza());
                put(order.getId(), order);
                order = new Order(new Customer("Kellie", "Carter", "(798) 346-2365"),
                        new CheesePizza());
                put(order.getId(), order);
                order = new Order(new Customer("Jessie", "Chandler", "(594) 654-5641"),
                        new CarbonaraPizza());
                put(order.getId(), order);
            }
        };
    }

    @Override
    public void save(Order order) {
        orders.put(order.getId(), order);
    }

    @Override
    public void update(long id, Order order) {
        orders.put(id, order);
    }

    @Override
    public void delete(long id) {
        orders.remove(id);
    }

    @Override
    public SortedMap<Long, Order> getOrders() {
        return orders;
    }

    @Override
    public Order findOneById(long id) {
        return orders.get(id);
    }
}
