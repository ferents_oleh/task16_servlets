package com.epam.service;

import com.epam.model.Order;
import com.epam.model.Pizza;
import com.epam.payload.OrderRequest;

import java.util.SortedMap;

public interface OrderService {
    void save(Order order);

    void update(long id, OrderRequest orderRequest);

    void delete(long id);

    SortedMap<Long, Order> findAll();

    Pizza getPizzaImplementation(String pizza);
}
