package com.epam.service.impl;

import com.epam.dao.OrderDao;
import com.epam.enums.PizzaType;
import com.epam.model.Customer;
import com.epam.model.Order;
import com.epam.model.Pizza;
import com.epam.model.impl.CarbonaraPizza;
import com.epam.model.impl.CheesePizza;
import com.epam.model.impl.VeganPizza;
import com.epam.payload.OrderRequest;
import com.epam.service.OrderService;

import java.util.SortedMap;

public class OrderServiceImpl implements OrderService {
    private OrderDao orderDao;

    public OrderServiceImpl(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    public void save(Order order) {
        orderDao.save(order);
    }

    @Override
    public void update(long id, OrderRequest orderRequest) {
        Order order = orderDao.findOneById(id);
        order.setId(id);
        Order requestOrder = orderRequest.getOrder();
        Customer requestCustomer = requestOrder.getCustomer();
        String pizzaImplementation = orderRequest.getPizzaClass();
        Pizza pizza;
        if (pizzaImplementation != null) {
            pizza = getPizzaImplementation(pizzaImplementation);
            order.setOrderedPizza(pizza);
        }
        if (requestCustomer != null) {
            if (requestCustomer.getFirstName() != null) {
                order.getCustomer().setFirstName(requestCustomer.getFirstName());
            }
            if (requestCustomer.getLastName() != null) {
                order.getCustomer().setLastName(requestCustomer.getLastName());
            }
            if (requestCustomer.getPhoneNumber() != null) {
                order.getCustomer().setPhoneNumber(requestCustomer.getPhoneNumber());
            }
        }
        orderDao.update(id, order);
    }

    @Override
    public void delete(long id) {
        orderDao.delete(id);
    }

    @Override
    public SortedMap<Long, Order> findAll() {
        return orderDao.getOrders();
    }

    @Override
    public Pizza getPizzaImplementation(String orderedPizza) {
        Pizza pizza = null;
        if (orderedPizza.equalsIgnoreCase(PizzaType.Cheese.toString())) {
            pizza = new CheesePizza();
        } else if (orderedPizza.equalsIgnoreCase(PizzaType.Vegan.toString())) {
            pizza = new VeganPizza();
        } else if (orderedPizza.equalsIgnoreCase(PizzaType.Carbonara.toString())) {
            pizza = new CarbonaraPizza();
        }
        return pizza;
    }
}
