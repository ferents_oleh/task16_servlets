package com.epam.enums;

public enum PizzaType {
    Cheese, Vegan, Carbonara
}
