package com.epam.controller;

import com.epam.dao.impl.OrderDaoImpl;
import com.epam.model.Customer;
import com.epam.model.Order;
import com.epam.model.Pizza;
import com.epam.payload.OrderRequest;
import com.epam.service.OrderService;
import com.epam.service.impl.OrderServiceImpl;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;

@WebServlet("/orders/*")
public class SaveOrderController extends HttpServlet {
    private OrderService orderService;

    private Gson gson;

    @Override
    public void init() throws ServletException {
        orderService = new OrderServiceImpl(
                new OrderDaoImpl()
        );
        gson = new Gson();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        req.setAttribute("orders", orderService.findAll());
        req.getRequestDispatcher("orders.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String orderedPizza = req.getParameter("pizza");

        Pizza pizza = orderService.getPizzaImplementation(orderedPizza);
        Order order = new Order(
                new Customer(req.getParameter("firstName"), req.getParameter("lastName"), req.getParameter("phoneNumber")),
                pizza
        );
        orderService.save(order);
        resp.sendRedirect("/orders");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getPathInfo();

        String[] splits = path.split("/");
        String id = splits[1];

        StringBuilder buffer = new StringBuilder();
        BufferedReader reader = req.getReader();
        String line;
        while((line = reader.readLine()) != null) {
            buffer.append(line);
        }
        long orderId = Long.parseLong(id);
        OrderRequest orderRequest = gson.fromJson(buffer.toString(), OrderRequest.class);
        orderService.update(orderId, orderRequest);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getPathInfo();

        String[] splits = path.split("/");
        String id = splits[1];
        orderService.delete(Long.parseLong(id));
    }
}
